" Buffers Keybindings: no plugin required

nnoremap <C-b> <Nop>
nnoremap <C-b><C-b> :buffers<CR>:buffer<Space>
nnoremap <C-b><C-d> :buffers<CR>:bdelete<Space>
nnoremap <C-b><C-x> <Cmd>bdelete<CR>

