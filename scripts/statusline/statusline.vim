" Statusline: Simple statusline, no plugin required

set statusline=

"	Left:

"		Mode:
set statusline+=\ 
set statusline+=%{StatuslineMode()}

"		Gitbranch:
if get(g:, 'show_gitbranch', 0)
	set statusline+=%{b:gitbranch}
endif

set statusline+=\ \|

"	full-path, modified and read-only
set statusline+=\ %F\ %m\ %r

"	Right:
set statusline+=%=

"	file-type and cursor information
set statusline+=\ %y\ %l:%v\ \ %L\ 

" As You May See: Some feateures make irrelevant to use some options
set noruler    " Disable relative info of line
set noshowmode " Disable showing MODE below

