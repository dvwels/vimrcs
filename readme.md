VIMRC and .VIM directory

After clonning this respository.

1. Install Vim-Plug:

+ Unix:
```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

+ Windows (with `set rtp=~/.vim`):
	
```
iwr -useb https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim |`
    ni $HOME/.vim/autoload/plug.vim -Force
```

+ Windows (Default):
```
iwr -useb https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim |`
    ni $HOME/vimfiles/autoload/plug.vim -Force
```

NOTE: `rtp` means `runtimepath`. I've changed mine to match Unix's default.

