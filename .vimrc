" VIMRC: Configuration file for Vim
"
" 	Just a basic text editor, not trying to build
" 	 an IDE, instead be compatible even with TTY
"

"	Windows Only:
"	Force .vim instead of vimfiles
set nocp rtp^=~/.vim,~/.vim/after

"	Quick access method
command! Config :e $MYVIMRC
command! ConfigChangeDirectory :lcd ~/.vim

"	Main Settings:

"	Load your plugins
runtime settings/custom/set_vim_plug.vim

"	Load your vim options
runtime settings/custom/set_vim_options.vim

"	Load vim options for os and plataform {1}
let g:want_truecolor=1 " Caviats are described inside the file
runtime! settings/support/raw/*.vim

"	Load your color schemes
"
"	{1} `g:is_tty` is defined inside `set_plataform.vim`
"
if get(g:, 'is_tty', 0)
	colo elflord
else
	if g:want_truecolor " Apply some modifications
		runtime settings/custom/set_vim_colorscheme.vim
	endif
	colo srcery
endif

"	Remappings vim keys and some scripts
runtime settings/mappings/mappings.vim

let g:show_gitbranch=0 " Option for statusline
"	 this may slow down in startup when enabled
runtime! scripts/*/*.vim

"	Plugin Settings:

"	Select your <Leader> key
let mapleader='ñ'

runtime settings/plugins/easy_align.vim
runtime settings/plugins/netrw.vim
runtime settings/plugins/goyo.vim

"	Others:

"	Specific tweaks for terminals
runtime settings/support/fix/set_alacritty.vim

