" Alt Keybindings:

"		To handle panel movement
tnoremap <M-w> <C-w>
"		Some plugins (eg. Goyo.vim) may need remap
"		 these keys, so it needs to be recursive
nmap <M-w> <C-w>
vnoremap <M-w> <C-w>
inoremap <M-w> <Esc><C-w>

"		To move line with cursor
nnoremap <M-Up> :m .-2<CR>==
vnoremap <M-Up> :m '<-2<CR>gv=gv
inoremap <M-Up> <Esc>:m .-2<CR>==gi
nnoremap <M-Down> :m .+1<CR>==
inoremap <M-Down> <Esc>:m .+1<CR>==gi
vnoremap <M-Down> :m '>+1<CR>gv=gv

" Esc Keybindings:

"		To escape terminal mode
tnoremap ¿<Esc> <C-w>N

