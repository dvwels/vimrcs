" PLUGIN MANAGER: VIM-PLUG
"
"	Note that some of these plugins may be
"	 outdated but completely functional
"

call plug#begin('~/.vim/bundle')             " Here ur plugins will be stored
Plug 'junegunn/vim-plug', {'on':'help plug'} " Getting help page for vim-plug

"	General Purpose:

Plug 'dstein64/vim-startuptime', {'on':'StartupTime'}

"	Easy Writing:

Plug 'junegunn/vim-easy-align'                     " Autoalign text based on
                                                   "  delimiter keys
Plug 'https://gitlab.com/dvwels/notsogoyo.vim.git' " Fork of goyo not so calm

"	Improving Vim:

Plug 'junegunn/vim-peekaboo'            " Easier way to manage of registers

"	Software Required:

Plug 'mhinz/vim-signify'

"	Color Schemes:

Plug 'srcery-colors/srcery-vim', {'as':'cs/srcery'}

"	That's all right?"
call plug#end()

