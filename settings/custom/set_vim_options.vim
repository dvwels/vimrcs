" VIM SETTINGS: 

"	Some may be default in NeoVim ----------------------------------------

"	Win To Unix:
set encoding=utf-8             " Encoding that is shown
set fileformats=unix,dos       " Use LF (\n) instead of CRLF (\r\n)
set backspace=indent,eol,start " Allow backspace working OK

"	General Behaviour:
filetype plugin on  " Enable ft detection and reads lang.vim
syntax on           " Enable sintax highlighting based on the language

set autoread        " Read changes made in another editor than Vim
set laststatus=2    " Enable line status, by default is 1

set wildmenu        " Enables a better looking selection menu
set wildoptions=pum " Enables pop-up-menu (aka. vertical menu)

"	Recovery Behaviour:
set directory=~/.vim/tmp// " Directory for swap files
set nobk nowb              " No backups saved nor while editing in vim

"	About Searching:
set hlsearch     " Highlight when searching
set incsearch    " Incremental search, it's more visual
set shortmess-=S " Show number of concurrences of a search
set shortmess+=F " No file info while editing

"	About Indentation:
set autoindent " Enable automatic indentation of files
set smarttab   " Fill with spaces or tabs smartly

"	Other Ones:
set belloff=all complete-=i
set history=1000 sidescroll=1
set cscopeverbose formatoptions=tcqj
set nojoinspaces langnoremap nolangremap
set viewoptions-=options sessionoptions-=options
set nofsync hidden ttyfast nostartofline
set display=lastline switchbuf=uselast
set listchars+=tab:»\ ,space:·,trail:-,nbsp:+

"	For Vim and NeoVim ---------------------------------------------------

"	General Behavior:

set updatetime=50       " Writes the swap file after Xms of inactivity
set completeopt=menuone " Properties of ins-completion pop-up menu

"	Split Behavior:

set splitright " When split, cursor go to right
set splitbelow " When vsplit, cursor goes down

"	Scrolling Behavior:

set mouse=a     " Supossed to make scroll work
set scrolloff=3 " Supossed to make scroll work

"	About Searching:

set ignorecase " Turn off case sensitive searching
set smartcase  " Turn on case sensitive when uppercase letters

"	About Line Number:

set number         " Display line number
set relativenumber " Uses relative numbers

"	Appearance:

set cursorline     " Highlight the line your cursor is in
set colorcolumn=80 " Highlight the column you set

