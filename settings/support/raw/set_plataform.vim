" Based On Plataforms: set options you'll be happy with
"
"	Note that TTY has only 16 color support
"
"	Note that not all terminals have true color
"	 support, by default is 256 color
"
"	Note that for Vim, the only GUI plataform
"	 supported by the Vim backend is gVim
"

"	If Run On: TTY
"		With 16 color support
"
if &term == 'linux'
	let g:is_tty=1
	set notermguicolors

"	If Run On: Terminal emulators
"
elseif !has('gui_running')
	let g:is_tty=0

"		With 256 color support
	let s:want_truecolor=get(g:, 'want_truecolor', 0) 
	if !s:want_truecolor
		set t_Co=256

"		With true color support
	elseif has('termguicolors') && s:want_truecolor
		set termguicolors
	endif

"	If Run On: GUI forces termguicolors to be enabled
"
else
	let g:is_tty=0
	set guioptions=c " Console like menu; No menu/tool/scroll bar
	set guifont=JetBrainsMono_NF:h14,Consolas:h14 " Fonts
endif

