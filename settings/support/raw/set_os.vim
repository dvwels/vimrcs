" Based On OS: set options you'll be happy with
"
"	Don't want to load a different branch if using
"	 another OS, so specific settings are here
"

"	Linux Options:
if has('unix')
	set clipboard=unnamedplus " Use the system clipboard
	runtime settings/support/fix/set_externals.vim

"	Mac Options:
	if has('mac')
		set clipboard=unnamed " Use the system clipboard
	endif

"	Windows Options:
elseif has('win64') || has('win32')
	set fileformats=unix,dos,mac " Preferred order
	set clipboard=unnamed        " Use the system clipboard
	if empty(glob('~/.vim/autoload/plug.vim'))	
		set shell=powershell     " Do not use this property if using vim-plug
	endif
endif

