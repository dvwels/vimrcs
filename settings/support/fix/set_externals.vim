" Vim External Tools Options:
"
"	Some programs use vim to open things,
"	 so this is the file to configure it
"

"	VimPager Settings: Use pager even with really small text
let vimpager_passthrough=0

