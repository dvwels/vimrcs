" Options for .VIM files

"	HOW TO MAKE COMMENT:?
let b:line_comment="\""

"	INDENT: THESE SETTINGS ALLOW JUST TO USE TAB's
setlocal tabstop=4    " Indent 4 spaces when manual <Tab>
setlocal shiftwidth=4 " Indent 4 spaces when use '>' or '<' for <Tab>
"setlocal autoindent   " Well, it's name already tell what it does
"set noexpandtab  " IF SET: Fill with tab, otherwise with spaces

